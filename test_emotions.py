import re
from functools import partial
from typing import Tuple, Generator, Optional, Iterable
import pymorphy2

from settings import EMO_DICT_PATH
from utils import Emotions


__all__ = ('find_emotions', 'get_text_rate')

emotions = Emotions(open(EMO_DICT_PATH))
morph = pymorphy2.MorphAnalyzer(result_type=None)
names = {}


def _process(word: str, keywords: set) -> str:
    if '@' in word:
        keywords |= {word}
        return word
    word = word.strip()
    word = re.sub(r"^(\W*)(\w+'?\w+)(\W*)$", lambda x: x.groups()[1], word)
    if any(re.findall(r'[0-9]', word)):
        keywords |= {word}
    return word


def find_emotions(lines: Iterable[str]) -> Generator[Tuple[str, Optional[float]], None, None]:
    words_generator = (word for line in lines for word in line.split(' ') if word)
    keywords = set()
    process = partial(_process, keywords=keywords)
    for word_vars in map(lambda x: morph.normal_forms(process(x)), words_generator):
        for word in word_vars:
            if emotions.get(word):
                yield word, emotions[word]
            else:
                yield word, None


def get_text_rate(text: str) -> Tuple[float, float]:
    text_rate = 0.
    text_rate_max = 0
    for word, rate in find_emotions(text.split('\n')):
        if rate:
            text_rate += rate
            text_rate_max += 5
    return text_rate, text_rate_max
