import re
import numpy as np
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline

from stemmer import porter

docs_new = ['God is love', 'OpenGL on the GPU is fast']
categories = ['alt.atheism', 'soc.religion.christian', 'comp.graphics', 'sci.med']
twenty_train = fetch_20newsgroups(subset='train', categories=categories, shuffle=True, random_state=13)
twenty_test = fetch_20newsgroups(subset='test', categories=categories, shuffle=True, random_state=13)

keywords = set()
names = {}
wrong_stemming = {}


def process(word, iterator):
    global keywords
    if word[0].isupper() and names.get(word.lower()):
        next_word = next(iterator)
        keywords |= {' '.join((word, next_word))}
        return word, next_word
    if '@' in word:
        keywords |= {word}
        return word
    word = word.strip()
    word = re.sub(r"^(\W*)(\w+'?\w+)(\W*)$", lambda x: x.groups()[1], word)
    if any(re.findall(r'[0-9]', word)):
        keywords |= {word}
        return word
    else:
        return porter.stem(word)


def verbose(doc):
    iterator = iter(doc.split())
    return [process(s, iterator) for s in iterator]


count_vect = TfidfVectorizer(tokenizer=verbose)
x_train = count_vect.fit_transform(twenty_train.data)
with open('log.log', 'w') as f:
    for word in count_vect.vocabulary_.items():
        f.write(' '.join(str(x) for x in word) + '\n')
