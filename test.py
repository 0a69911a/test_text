import numpy as np
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline

docs_new = ['God is love', 'OpenGL on the GPU is fast']
categories = ['alt.atheism', 'soc.religion.christian', 'comp.graphics', 'sci.med']
twenty_train = fetch_20newsgroups(subset='train', categories=categories, shuffle=True, random_state=13)
twenty_test = fetch_20newsgroups(subset='test', categories=categories, shuffle=True, random_state=13)


# count_vect = CountVectorizer()
# x_train = count_vect.fit_transform(twenty_train.data)
#
# tf_transformer = TfidfTransformer()
# x_train_tf = tf_transformer.fit_transform(x_train)
#
# clf = MultinomialNB().fit(x_train_tf, twenty_train.data)

# x_new = count_vect.transform(docs_new)
# x_new_tf = tf_transformer.transform(x_new)
# predicted = clf.predict(x_new_tf)

text_clf = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', SGDClassifier())
])
text_clf.fit(twenty_train.data, twenty_train.target)
predicted = text_clf.predict(twenty_test.data)
r = np.mean(predicted == twenty_test.target)
