import csv
import marisa_trie
from typing import TextIO, Tuple, List


class EmotionsDialect(csv.unix_dialect):
    delimiter = ';'


csv.register_dialect('emotions_dialect', EmotionsDialect)


class Emotions:
    __slots__ = ('data', '_data_iter')

    def __init__(self, file: TextIO):
        _csv = csv.DictReader(file, dialect='emotions_dialect')
        self.data = {item['term'].lower(): float(item['value']) + 3 for item in _csv}
        self._data_iter = iter(self.data.items())

    def __getitem__(self, item):
        return self.data[item]

    def __setitem__(self, key, value):
        raise NotImplemented

    def __next__(self):
        return next(self._data_iter)

    def __iter__(self):
        return iter(self.data.items())

    def __getattr__(self, item):
        if item not in self.__slots__:
            return getattr(self.data, item)
        super(Emotions, self).__getattr__(item)


class SynonymsDialect(csv.unix_dialect):
    delimiter = ','


csv.register_dialect('synonyms_dialect', SynonymsDialect)


class Synonyms:
    _trie: marisa_trie.RecordTrie
    _synonyms_tuple: Tuple[tuple]
    __slots__ = ('_trie', '_synonyms_tuple', '_iter')

    def __init__(self, file: TextIO):
        def f(synonyms_list: list, _csv: csv.DictReader):
            for num, record in enumerate(_csv):
                words = record['words'].split(';')
                synonyms_list.append(tuple(words))
                for word in words:
                    yield (word, (num, ))

        tmp_list: List[tuple] = []
        _csv = csv.DictReader(file, dialect='synonyms_dialect')
        self._trie = marisa_trie.RecordTrie('I', f(tmp_list, _csv))
        self._synonyms_tuple = tuple(tmp_list)
        self._iter = iter(self._synonyms_tuple)

    def __getitem__(self, item):
        return [self._synonyms_tuple[num[0]] for num in self._trie[item]]

    def __iter__(self):
        return iter(self._synonyms_tuple)

    def __next__(self):
        return next(self._iter)
